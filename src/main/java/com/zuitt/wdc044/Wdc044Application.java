package com.zuitt.wdc044;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class Wdc044Application {

	public static void main(String[] args) {
		SpringApplication.run(Wdc044Application.class, args);
	}

//	@RequestParam - is used to extract query parameters, form parameters, and even files from the request.
//		http://localhost:8080/hello
	@GetMapping("/hello")
	public String hello(@RequestParam(value = "name", defaultValue = "World") String name) {
		return String.format("Hello %s!", name);
	}

	@GetMapping("/greetings")
	public String greetings(@RequestParam(value = "greet", defaultValue = "World") String greet) {
		return String.format("Good evening, %s! Welcome to Batch 254!", greet);
	}

//	Springboot Activity
	@GetMapping("/hi")
	public String hi(@RequestParam(value = "user", defaultValue = "John Doe") String user) {
		return String.format("hi %s!", user);
	}

	@GetMapping("/nameAge")
	public String nameAge(@RequestParam(value = "name", defaultValue = "Juan") String name,
						  @RequestParam(value = "age", defaultValue = "18") String age) {
		return String.format("Hello %s! Your age is %s.", name, age);
	}



}
