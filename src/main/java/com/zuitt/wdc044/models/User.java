package com.zuitt.wdc044.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Set;

//mark this Java object as a representation of a database table via @Entity
@Entity
//designate table name via @Table.
@Table(name="user")
public class User {

    //    indicate that this property represents the primary key via @Id
    @Id

//    values for this property will be auto-incremented - has only one system in numbering objects.
    @GeneratedValue
    private Long id;

    //    class properties that represent table columns in a rational database are annotated as @Column
    @Column
    private String username;

    @Column
    private String password;

    @OneToMany(mappedBy = "user")
//    to avoid infinite recursion, use the @JsonIgnore annotation.
    @JsonIgnore
    private Set<Post> posts;

    private Set<Post> getPosts() {
        return posts;
    }

    //    default constructor - this is needed when retrieving posts
    public User() {}

    public  User(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public Long getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
