package com.zuitt.wdc044.services;

import com.zuitt.wdc044.models.Post;
import org.springframework.http.ResponseEntity;

public interface PostService {
//    create a post

    void createPost(String stringToken, Post post);

//    getting all posts
    Iterable<Post> getPosts();

    Iterable<Post> getPostsByUser(String stringToken);

//    edit a user post

    ResponseEntity updatePost(Long id, String stringToken, Post post);

    ResponseEntity deletePost(Long id, String stringToken);


}
